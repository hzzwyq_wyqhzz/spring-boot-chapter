package org.minbox.chapter.springboot.constructor.binding.properties;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SpringbootConstructorBindingPropertiesApplicationTests {

    @Autowired
    private MinBoxConfig minBoxConfig;

    @Test
    void printConfig() {
        System.out.println("作者名称：" + minBoxConfig.getAuthor());
        System.out.println("作者博客地址：" + minBoxConfig.getBlogAddress());
    }

}
