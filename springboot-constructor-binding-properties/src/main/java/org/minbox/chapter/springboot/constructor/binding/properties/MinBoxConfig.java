package org.minbox.chapter.springboot.constructor.binding.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

import static org.minbox.chapter.springboot.constructor.binding.properties.MinBoxConfig.PREFIX;

/**
 * 配置类
 *
 * @author 恒宇少年
 */
@ConfigurationProperties(prefix = PREFIX)
@ConstructorBinding
public class MinBoxConfig {
    /**
     * 映射绑定 "minbox.config"前缀的配置信息
     */
    public static final String PREFIX = "minbox.config";
    /**
     * 配置信息：作者
     */
    private String author;
    /**
     * 配置信息：博客地址
     */
    private String blogAddress;

    public MinBoxConfig(String author, String blogAddress) {
        this.author = author;
        this.blogAddress = blogAddress;
    }

    public String getAuthor() {
        return author;
    }

    public String getBlogAddress() {
        return blogAddress;
    }
}
